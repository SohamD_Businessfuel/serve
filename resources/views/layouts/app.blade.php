<!DOCTYPE html>
<html lang="en">

<head>
	<!-- meta tags-->
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<title>Bidtend | Home</title>

	<!-- style files-->
	<link rel="stylesheet" href="css/bootstrap.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/media.css" />
	<link rel="stylesheet" href="css/font-awesome.min.css" />
         <link rel="stylesheet" href="css/custom.css" />
	<link rel="icon" href="images/fav.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
	<link rel="icon" href="images/fav.png">
	<!-- google fonts -->
	<!-- google fonts -->
	<link href='https://fonts.googleapis.com/css?family=Poppins:400,500,700,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="inputTags/css/inputTags.css">

	<link href='https://fonts.googleapis.com/css?family=Poppins:400,500,700,300,600' rel='stylesheet' type='text/css'>

	<!-- HTML5 shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->
</head>

<body>
	<!--header start-->
	<header>
		<!-- top bar start -->
		<div class="top-bar">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<span><a href="hre">Welcome to Bidtend</a></span>
						<span>
							<a href="/login" class="fa fa-user">&nbsp;&nbsp;<span style="font-family: Poppins;">Log in</a>
						</span>
						<span>
							<a href="/registration" class="fa fa-unlock-alt">&nbsp;&nbsp;<span style="font-family: Poppins;">Register</a>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<!-- top bar end -->

		<!-- navigation bar start -->
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
					<a class="navbar-brand" href="/"><img src="images/logo.png" alt="Bitdent"></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="index.html">home <span class="sr-only">(current)</span></a></li>
						<li><a href="explore.html">explore</a></li>
						<li><a href="startaproject.html">start a project</a></li>
						<li><a href="howitworks.html">How it Works?</a></li>
				
						<li><a href="contact.html">contact us</a></li>
						<li><a class="search-icon" href="#topmenusearch"><i class="fa fa-search"></i> </a></li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container -->
		</nav>
		<!-- navigation bar end -->
	</header>
	<!--header end-->
  <div class="fixed-remove"></div>
	<!-- search start-->
@yield('content')
	<!-- footer start -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-4">
					<div class="footer-middle-content">
						<p>Subscribe to our email newsletter &amp; receive updates</p>
						<div class="newsletter-box" style="margin-bottom:10px;">
							<form class="search-box">
								<div>
									<input type="text" name="s" id="search-letter" placeholder="Enter your e-mail">
									<button type="submit" id="search-letter-btn" class="btn btn-search">
									<i class="fa fa-paper-plane-o"></i></button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!--Links Start-->
				<div class="col-xs-6  col-md-3 col-md-offset-0">
					<div class="contact-area" style="margin-top:20px;">
						<div class="footer-info contact">
							<ul class="toggle-footer list-unstyled">
								<li>
									<p>About Us</p>
								</li>
							
								<li>
									<p>Advertise With Us</p>
								</li>
								<li>
									<p>How It Works</p>
								</li>
							</ul>

						</div>
					</div>
				</div>

				<div class="col-xs-6  col-sm-4 col-sm-offset-2 col-md-3 col-md-offset-0">
					<div class="contact-area" style="margin-top:20px;">
						<div class="footer-info contact">
							<ul class="toggle-footer list-unstyled">
								<li>
									<p>Terms &amp; Conditions</p>
								</li>
								<li>
									<p>Privacy Policy</p>
								</li>
								
								<li>
									<p>Contact Us</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!--Links End-->
                <!--Time Area Start-->
				<div class="col-xs-12 col-md-2">
					<div class="time-area text-center" style="margin-top:20px;">

						<p style="color:white;">Payment Methods</p>
						<div class="paypal">
							<img src="images/payment_skrill_2.png" alt="">
							<img src="images/payment_paypal.png" alt="">
						</div>
					</div>
				</div>
				<!--Time Area End-->
				<div class="clearfix"></div>
				<div class="copy">
					<div class="col-xs-12 col-sm-6">
						<p>Copyright <sup>&copy;</sup> <a href="#" target="_blank">Bidtend</a> All rights reserved.</p>
					</div>
					<div class="col-xs-8 col-sm-6 col-md-3 col-md-offset-3 social">
						<ul class="list-unstyled">
						<li>
							<a href="">
								<i class="fa fa-facebook" aria-hidden="true"></i>
							</a>
							<a href="">
								<i class="fa fa-google-plus" aria-hidden="true"></i>
							</a>
							<a href="">
								<i class="fa fa- fa-twitter" aria-hidden="true"></i>
							</a>
							<a href="">
								<i class="fa fa- fa-instagram" aria-hidden="true"></i>
							</a>
							<a href="">
								<i class="fa fa- fa-linkedin" aria-hidden="true"></i>
							</a>
						</li>
					</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- top search start -->
	<div id="topmenusearch">
		<button class="btn btn-danger" style="position: fixed;">
			<i class="fa fa-search "></i>&nbsp;&nbsp;
			<span>SEARCH</span>
		</button>
		<button type="button" class="close">×</button>
		<form>
			<div class="searcher">
				<input type="search" value="" placeholder="type keyword(s) here" />
			</div>			
			<div class="searchtagsrow">
				<ul class="tags">
					<span class="tag label label-info">X<span data-role="remove"></span></span>
                    <span class="tag label label-info">X<span data-role="remove"></span></span>
                    <span class="tag label label-info">X<span data-role="remove"></span></span>
				</ul>
			</div>
		</form>
	</div>
	<!-- top search end -->
	<!-- footer end -->

	<!-- scripts -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="inputTags/js/inputTags.jquery.js"></script>
	<script src="js/moment.js"></script>
	<script type="text/javascript" src="js/tagsinput.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/jquery.fittext.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
	<script>
    $(document).ready(function(){
        var date_input=$('input[name="date"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
    })
</script>
</body>

</html>