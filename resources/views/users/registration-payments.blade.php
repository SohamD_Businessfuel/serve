@extends('layouts.app')
@section('content')
<br> <br>
<!-- view  -->
<div class="list-page-heading mb95">
    <div class="container">
        <div class="section-heading  small-heading text-center">
            <h3>Review Your Order & Complete Payment</h3>
            <span>
                  Payments are Safe & Secure
                </span></div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb-single">
                    <ul class="breadcrumbs">
                        <li><a href="index.html" title="Return to Home">
                            <i class="fa fa-home"></i>
                            Home
                        </a></li>
                        <span>&gt;</span>
                        <li> Register</li>
                        <li>
                            <span>&gt;</span>
                        </li>
                        <li>Select Catogories</li>
                        <li>
                            <span>&gt;</span>
                        </li>
                        <li>Choose Pricing Plan</li>
                        <li>
                            <span>&gt;</span>
                        </li>
                        <li>Review & Payment</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<br> <br>
<section class="progress-barr">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="checkout-bar">
                    <li class="active">
                        <a href="#">Register</a>
                    </li>

                    <li class="active">Select Catogories</li>

                    <li class="active">Choose Pricing Plan</li>

                    <li class="active">Review &amp; Payment</li>

                    <li class="">Complete</li>

                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="single-item">
            <div class="candidateheading">
                <h4 class="orange">Review
                    Your Order</h4>
            </div>
            <div class="content">
                <p>


                    <div class="buy">
                        <div class="row">
                            <div class="col-sm-4 col-xs-4">
                                <div class="diamond"><a href="index.html#"><img src="images/bidcoin.png"
                                                                                class="img-responsive"></a>
                <p><strong>1000</strong>&nbsp;Credits</p>
            </div>
        </div>
        <div class="col-sm-4 col-xs-4"></div>
        <div class="col-sm-4 col-xs-4">
            <div class="best">

                <p>
                <div class="total total-fix">for $ 70.00</div>
            </div>
        </div>
    </div>
</div>

<div class="payment-method row">
        <div style="text-align: center;">
            <center><h3>Payment Method</h3></center>


            <br>
            <div class="col-md-6">
                <label for="card" class="method card">
                    <div class="card-logos">
                        <img src="https://www.payfast.co.za/storage/PayFast-logo.png" width="200" height="50">
                    </div>

                    <div class="radio-input">
                        <input id="card" type="radio" name="payment" required>
                        Pay $147.00 with Payfast</div>
                </label>
            </div>
            <div class="col-md-6">
                <label for="paypal" class="method paypal">
                    <img src="images/paypal_logo.png">
                    <div class="radio-input">
                        <input id="paypal" type="radio" name="payment" required>
                        Pay $147.00 with PayPal
                    </div>
                </label>
            </div>
        </div>
    </p>
</div>
</form>
</div>   </div>
</div>

<!-- End ul#plans -->


</section>

<div class="container" style="padding: 30px 0px">
    <div class="row">
        <div class="col-md-4">
            <a href="/registration-plans" class="explore-btn">
                <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;
                <span>BACK</span>
            </a>
        </div>
        <div class="col-md-4 text-center">

        </div>
        <div class="col-md-4 text-right">
            <!-- <button type="submit" class="explore-btn">
                <i class="fa fa-arrow-right"></i>&nbsp;&nbsp;
                <span>NEXT</span>

            </button> -->
            <a href="/registration-complete" class="explore-btn">
                <i class="fa fa-arrow-right"></i>&nbsp;&nbsp;
                <span>NEXT</span>

            </a>
        </div>
    </div>
</div>


</div>
</div>
</p>
</div>
</div>


</form>

<!-- end view-->
@endsection