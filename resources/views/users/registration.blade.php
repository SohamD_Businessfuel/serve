@extends('layouts.app')
@section('content')
    <div class="fixed-remove"></div>
    <!-- start project-->
     <div class="list-page-heading mb100">
        <div class="container">
            <div class="section-heading  small-heading text-center">
                <h3>CREATE YOUR PROFILE</h3>
                <span>
                    The best opportunity is waiting
                </span>
            </div>
        </div>
    </div>
 <form method="POST" action="{{ route('register') }}">
    {{ csrf_field() }}
     <!-- Progress Bar Start -->
     <section class="progress-barr">
         <div class="container">
             <div class="row">
                 <div class="col-md-12">
                     <ul class="checkout-bar">
                         <li class="active">
                             <a href="#">Register</a>
                         </li>

                         <li class="">Select Catogories</li>

                         <li class="">Choose Pricing Plan</li>

                         <li class="">Review &amp; Payment</li>

                         <li class="">Complete</li>

                     </ul>
                 </div>
             </div>
         </div>
     </section>
     <!-- Progress Bar End -->
     <div class="post-job-body">
         <div class="container">
             <div class="row">
                 <div class="col-md-12">
                     <div class="route-upload-file pdt10">
                         <div class="image mt0">
                             <input id="company-picture" name="sort-by"  type="file" accept="image/*" class="file validate[required]">
                             <img style="display:none;width: 75px;height: 75px; border: 1px solid black;border-radius: 100%;" onclick="$('#company-picture').click()" id="open_img">
                             <label for="company-picture"><i class="fa fa-user-circle" id="open_label"></i></label>
                         </div>
                         <span>Add your Profile picture</span>

                     </div>
                 </div>
             </div>
             <div class="post-job-body">
    <div class="container">
        <div class="row">
            <div class="col12">
                <div class="company-create-form mb100">
                    <form action="#">

                    <div class="left-col col-md-6" style="padding-right: 40px;">
                                  
                            <div class="col-md-12 col-sm-12">
                                <div class="create-from-input-item">
                                    <label>Name*</label>
                                         <div class='input-group'>
                                             <input type='text' name="name"class="form-control" placeholder="Your Name" required/>
                                            <span class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                    </div>
                                </div><!--/.create-from-input-item-->
                            </div>

                            <div class="col-md-12 col-sm-12">
                                <div class="create-from-input-item">
                                    <label>Surname*</label>
                                         <div class='input-group'>
                                             <input type='text' class="form-control" placeholder="Your Surname" required/>
                                            <span class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                    </div>
                                </div><!--/.create-from-input-item-->
                            </div>

                            <div class="col-md-12 col-sm-12">
                                <div class="create-from-input-item">
                                    <label>Gender</label>
                                    <div class="input-group">
                                        <!-- <input type='text' class="form-control" placeholder="Accounting Information Service"/> -->
                                        <div class="dropdown-select-arrow">
                                            <select class="form-control">
                                                <option value="male">Male</option>
                                                <option value="female">Female/option>
                                                
                                            </select>
                                            <i class="fa fa-angle-down right-side-icon"></i>
                                        </div>
                                    </div>
                                </div><!--/.create-from-input-item-->
                            </div>

                             <div class="col-md-12 col-sm-12">
                                <div class="create-from-input-item">
                                  <label>Date Of Birth</label>
                                    <div class="input-group date" id="datetimepicker">
                                                 <!-- Form code begins -->
                                    <input class="form-control" id="date" name="date" placeholder="MM/DD/YYY" type="text">
                                        <span class="input-group-addon">
                                        <i class="fa fa-calendar-check-o"></i>
                                    </div>
                                </div><!--/.create-from-input-item-->
                            </div>

                                
                            <div class="col-md-12">
                                 <div class="add-skill pdt20">
                                    <h4>Add Skills*</h4>
                                            <div class="create-from-input-item">
                                                <label>Keyword</label>
                                                <div class="input-group">
                                                    <input id="in_skill" data-role="tagsinput" type="text" class="form-control"
                                                           placeholder="Type skills needed">
                                                        

                                                    <span class="input-group-addon">
                                                        <button type="button" onclick="onPlusSkill()"><i class="fa fa-plus-circle"></i></button>
                                                    </span>
                                                </div>

                                            </div><!--/.create-from-input-item-->
                                   

                                        <div class="col-md-6">
                                            <div class="require-skill mt35">
                                                <p id="skill_tag_list">

                                                </p>
                                            </div>
                                        </div>

                        
                                </div>
                            </div>

                        </div>
                            




                        <div class="right-col col-md-6" style="border-left: 2px solid #DCDCDC; padding-left: 40px;">
                  
                                 <div class="col-md-12 col-sm-12">
                                <div class="create-from-input-item">
                                   <label>Email*</label>
                                             <div class='input-group'>
                                                 <input type='email' name="email" class="form-control" placeholder="contact@me.com" required/>
                                                <span class="input-group-addon">
                                                    <i class="fa fa-envelope-o"></i>
                                                </span>
                                    </div>
                                </div><!--/.create-from-input-item-->
                            </div>
                         


                                
                           <!--       <div class="col-md-6 col-sm-6">
                                    <div class="create-from-input-item">
                                        <label>Experience</label>
                                        <div class="input-group">
                                            <div class="dropdown-select-arrow">
                                                <select class="form-control">
                                                    <option value="1">0 - 1 Year</option>
                                                    <option value="2">0 - 2 Year</option>
                                                    <option value="3">0 - 3 Year</option>
                                                </select>
                                                <i class="fa fa-angle-down right-side-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->

                                
                           <div class="col-md-12 col-sm-12">
                                    <div class="create-from-input-item">
                                       <label>Website</label>
                                             <div class='input-group'>
                                                 <input type='url' class="form-control" placeholder="www.me.com"/>
                                                 <span class="input-group-addon">
                                                    <i class="fa fa-globe"></i>
                                                </span>
                                        </div>
                                    </div><!--/.create-from-input-item-->
                                </div>

                         

                              <div class="col-md-12 col-sm-12">
                                    <div class="create-from-input-item">
                                       <label>Mobile*</label>
                                             <div class='input-group'>
                                                 <input type='text' class="form-control" placeholder="072 111 111" required/>
                                               <span class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </span>
                                        </div>
                                    </div><!--/.create-from-input-item-->
                                </div>




                                  <div class="col-md-12 col-sm-12">
                                    <div class="create-from-input-item">
                                      <label>Password*</label>
                                             <div class="input-group">
                                                 <input type="text" class="form-control" placeholder="eg. ****" required>
                                                 <span class="input-group-addon">
                                                <i class="fa fa-asterisk"></i>
                                            </span>
                                        </div>
                                    </div><!--/.create-from-input-item-->
                                </div>
   

                                 <div class="col-md-12 col-sm-12">
                                    <div class="create-from-input-item">
                                     <label>Password Again*</label>
                                             <div class="input-group">
                                                 <input type="text" name="password" class="form-control" placeholder="eg. ****" required>
                                                 <span class="input-group-addon">
                                                <i class="fa fa-asterisk"></i>
                                            </span>
                                        </div>
                                    </div><!--/.create-from-input-item-->
                                </div> </div> </div>

                                   
                      
                                    
                                     
                                    
                                 </div>
                             </div>
                             
                             <div class="short-description">
                                 <h4>Write Career Summary*</h4>
                                 <textarea  rows="5" cols="40" placeholder="Type short description to let people know what your company offers......" required></textarea>
                             </div>

                            

                                 </div><!--/.create-from-input-item-->
                             </div>

                              <div class="checkbox">
                                 <label>
                                      <input class="tgl tgl-light" id="cb1" type="checkbox"/>By clicking next you agree to the Bidtend <a href="terms.html">Terms &amp; Conditions</a>
                                 </label>
                             </div>

                             <div class="container" style="padding: 2px 0px">
                                 <div class="row">
                                     <div class="col-md-4">

                                     </div>
                                     <div class="col-md-4 text-center">

                                     </div>
                                     <div class="col-md-4 text-right">
                                         <button type="submit" class="explore-btn">
                                             <i class="fa fa-arrow-right"></i>&nbsp;&nbsp;
                                             <span>NEXT</span>

                                         </button>
                                         <!-- <a href="/registration-category" class="explore-btn">
                                             <i class="fa fa-arrow-right"></i>&nbsp;&nbsp;
                                             <span>NEXT</span>

                                         </a> -->
                                     </div>
                                 </div>
                             </div>
                         </form>
                     </div>
                 </div>
             </div>
         </div>
     </div>


 </form>

                     </div>
                 </div>
             </div>
         </div>
     </div>


 </form>
@endsection