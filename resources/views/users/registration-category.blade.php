@extends('layouts.app')
@section('content')
    <br> <br>
    <!-- view  -->
      <div class="list-page-heading mb95">
        <div class="container">
            <div class="section-heading  small-heading text-center">
                <h3>Please select the categories that appeal to you</h3>
                <span>
                  Tell us what you into
                </span></div>
        
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="breadcrumb-single">
                            <ul class="breadcrumbs">
                                <li><a href="index.html" title="Return to Home">
                                    <i class="fa fa-home"></i>
                                    Home
                                </a></li>  <span>&gt;</span>
                                <li> Register </li>
                                <li>
                                    <span>&gt;</span>
                                </li>
                                <li>Select Categories</li>
                            </ul>
                        </div>
                    </div>
                </div>
                        </div>
            </div>
		<section class="progress-barr">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="checkout-bar">
                        <li class="active">
                            <a href="#">Register</a>
                        </li>

                        <li class="active">Select Catogories</li>

                        <li class="">Choose Pricing Plan</li>

                        <li class="">Review &amp; Payment</li>

                        <li class="">Complete</li>

                    </ul>
                </div>
            </div>
        </div>
    </section>

<form action="#">
<div class="container">
   
      <div class="row">
        <div class="col-md-10 col-md-offset-1 text-center">
          <div class="circle-main">
         <div class="circle-div circle-div-bluse">
           <button type="button" class="btn-circle orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Consultants">Consultants</button>
         </div>
          <div class="circle-div circle-div-pop">
            <button type="button" class="btn-circle orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Building & Trades">Building & Trades</button>
          </div>
          <div class="circle-div circle-div-hip-pop">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">Civil</button>
          </div>
         <div class="circle-div circle-div-reegae">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">Cleaning & Facility Management</button>
          </div>
          <div class="circle-div circle-div-jazz">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">IT & Telecoms</button>
          </div>
          <div class="circle-div circle-div-rb">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">Accounting, Banking & Legal</button>
          </div>
         <div class="circle-div circle-div-classic-rock">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">General, Property, Auctions</button>
         </div>
          <div class="circle-div circle-div-metal">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">Engineering Consultants</button>
          </div>
          <div class="circle-div circle-div-electronic">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">Electrical & Automation</button>
          </div>
          <div class="circle-div circle-div-experimental">
           <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">Materials, Supply & Services</button>
          </div>
          <div class="circle-div circle-div-oldies">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">A Mechanical, Plant & Equipment</button>
          </div>
          <div class="circle-div circle-div-rock">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">Media & Marketing</button>
          </div>
          <div class="circle-div circle-div-classical">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">Medical & Healthcare</button>
          </div>
         <div class="circle-div circle-div-latin">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">Security, Access</button>
          </div>
          <div class="circle-div circle-div-hits">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil"> Alarms, Fire</button>
          </div>
          <div class="circle-div circle-div-indie">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">Indie</button>
          </div>
          <div class="circle-div circle-div-christian">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">Christian &amp;<br>Gospel</button>
          </div>
          <div class="circle-div circle-div-alternative">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">Alternative</button>
          </div>
          <div class="circle-div circle-div-dance">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">Dance</button>
          </div>
          <div class="circle-div circle-div-country">
            <button type="button" class="orange-circle-button" ><input class="categoty-checkbox" type="checkbox" name="category" value="Civil">Country</button>
          </div>
          </div>
        </div>
      </div>
    </div>

	<!--<div class="text-center">-->
		<!--<input type="text" name="categoty-required" class="category-required-check" oninvalid="this.setCustomValidity('Please Select At least One Category')" required/>-->
	<!--</div>-->
		                	
                         <div class="container" style="padding: 30px 0px">
      <div class="row">
           <div class="col-md-4">
            <a href="/registration" class="explore-btn">
					<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;
					<span>BACK</span>
				</a>
           </div>
            <div class="col-md-4 text-center">
              <button type="button" onclick="onReset()" class="explore-btn">
					<i class="fa fa-undo"></i>&nbsp;&nbsp;
					<span>RESET</span>
				</button>
           </div>
           <div class="col-md-4 text-right">
             <button id="category-submit" type="submit" class="explore-btn" disabled>
					<i class="fa fa-arrow-right"></i>&nbsp;&nbsp;
					<span>NEXT</span>
		
				</button>

        <a href="/registration-plans" class="explore-btn" disabled>
          <i class="fa fa-arrow-right"></i>&nbsp;&nbsp;
          <span>NEXT</span>
    
        </a>

           </div>
      </div>
    </div>

</form>
                        
                        </div>
					</div>
                    </p>
                </div>
            </div>
            
            

            
 @endsection          
<!-- end view-->