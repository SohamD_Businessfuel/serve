@extends('layouts.app')
@section('content')
<br> <br>

<form>
    <!-- pricing -->
    <div class="list-page-heading mb95">
        <div class="container">
            <div class="section-heading  small-heading text-center">
                <h3>Select Your Pricing Plan</h3>
                <span>
                  Get the most value for your money
                </span></div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb-single">
                        <ul class="breadcrumbs">
                            <li><a href="index.html" title="Return to Home">
                                <i class="fa fa-home"></i>
                                Home
                            </a></li>
                            <span>&gt;</span>
                            <li> Register</li>
                            <li>
                                <span>&gt;</span>
                            </li>
                            <li> Select Catogories</li>
                            <li>
                                <span>&gt;</span>
                            </li>
                            <li>Pricing Plans</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="progress-barr">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="checkout-bar">
                        <li class="active">
                            <a href="#">Register</a>
                        </li>

                        <li class="active">Select Catogories</li>

                        <li class="active">Choose Pricing Plan</li>

                        <li class="">Review &amp; Payment</li>

                        <li class="">Complete</li>

                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="col-md-8 col-md-offset-2">

        <section id="pricePlans">
            <ul id="plans">
                <li class="plan">
                    <ul class="planContainer">
                        <li class="title"><h2>Plan 1</h2></li>
                        <li class="price"><p>$10/<span>month</span></p></li>
                        <input class="plan-input" value="1" type="radio" name="plan" required>
                        <li>
                            <ul class="options">
                                <li>30 <span>Credits</span></li>
                            </ul>
                        </li>
                        <ul>
                        <li> Lorem Dummy </li>
                            <li> Lorem Dummy </li>
                                <li> Lorem Dummy </li>
                                    <li> Lorem Dummy </li>
                                        <li> Lorem Dummy </li>
                        </ul>
                        <li class="button"><a>Select</a></li>
                    </ul>
                </li>

                <li class="plan">
                    <ul class="planContainer">
                        <li class="title"><h2 class="bestPlanTitle">Plan 2</h2></li>
                        <li class="price"><p class="bestPlanPrice">$20/month</p></li>
                        <input class="plan-input" value="2" type="radio" name="plan" required>
                        <li>
                            <ul class="options">
                                <li>100 <span>Credits</span></li>
                            </ul>
                        </li>
                        <ul>
                        <li> Lorem Dummy </li>
                            <li> Lorem Dummy </li>
                                <li> Lorem Dummy </li>
                                    <li> Lorem Dummy </li>
                                        <li> Lorem Dummy </li>
                        </ul>
                        <li class="button"><a class="bestPlanButton">Select</a></li>
                    </ul>
                </li>

                <li class="plan">
                    <ul class="planContainer">
                        <li class="title"><h2>Plan 3</h2></li>
                        <li class="price"><p>$30/<span>month</span></p></li>
                        <input class="plan-input" value="3" type="radio" name="plan" required>
                        <li>
                            <ul class="options">
                                <li>70 <span>Credits</span></li>

                            </ul>
                        </li>
                        <ul>
                        <li> Lorem Dummy </li>
                            <li> Lorem Dummy </li>
                                <li> Lorem Dummy </li>
                                    <li> Lorem Dummy </li>
                                        <li> Lorem Dummy </li>
                        </ul>
                        <li class="button"><a>Select</a></li>
                    </ul>
                </li>
            </ul>
            <!-- End ul#plans -->


        </section>

    </div>

    <div class="container" style="padding: 30px 0px">
        <div class="row">
            <div class="col-md-4">
                <a href="/registration-category" class="explore-btn">
                    <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;
                    <span>BACK</span>
                </a>
            </div>
            <div class="col-md-4 text-center">

            </div>
            <div class="col-md-4 text-right">
                <!-- <button type="submit" class="explore-btn">
                    <i class="fa fa-arrow-right"></i>&nbsp;&nbsp;
                    <span>NEXT</span>

                </button> -->
                <a href="/registration-payments" class="explore-btn">
                    <i class="fa fa-arrow-right"></i>&nbsp;&nbsp;
                    <span>NEXT</span>

                </a>
            </div>
        </div>
    </div>


    </div>
    </div>
    </p>
    </div>
    </div>


</form>

<!-- end view-->
@endsection