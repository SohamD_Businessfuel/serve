@extends('layouts.app')
@section('content')
	<section class="search text-center">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="search-area">
					<h2>Browse Our Tenders & Contracts</h2>
					<!-- form inline start -->
					<form class="col-md-12 form-inline visible-md visible-lg">
						<div class="form-group">
							<div class="form-group">
								<input type="text" class="form-control  input-lg" id="looking-for" placeholder="I'm Lookin For">
							</div>
						</div>

						<div class="form-group">
							<select class="form-control input-lg">
								<option>All Categories</option>
								<option>Accounting, Banking &amp; Legal</option>
								<option>Building &amp; Trades</option>
								<option>Civil</option>
								<option>Cleaning &amp; Facility Management</option>
								<option>Engineering Consultants</option>
								<option>Electrical &amp; Automation</option>
								<option>General, Property, Auctions</option>
								<option>IT &amp; Telecoms</option>
								<option>Materials, Supply &amp; Services</option>
								<option>Mechanical, Plant &amp; Equipment</option>
								<option>Media &amp; Marketing</option>
								<option>Medical &amp; Healthcare</option>
								<option>Security, Access, Alarms, Fire</option>
							</select>
						</div>


						<div class="form-group">
							<select class="form-control input-lg">
								<option>All Locations</option>
								<option>Eastern Cape</option>
								<option>Free State</option>
								<option>Gauteng</option>
								<option>Kwazulu-Natal</option>
								<option>Limpopo</option>
								<option>     Mpumalanga</option>
								<option>Northen Cape</option>
								<option>North West</option>
								<option>Western Cape</option>
                       	    </select>
						</div>

						<div class="form-group">
							<a href="cart.html" class="cart-btn input-lg">
								<i class="fa fa-search"></i>
								<span>Search</span></a>
						</div>
					</form>
					<!-- form inline end -->


					<!-- form horizontal start -->
					<form class="form-horizontal col-xs-12 visible-xs visible-sm">
						<div class="form-group">
							<input type="text" class="form-control  input-lg" id="looking-for" placeholder="I'm Lookin For">
						</div>

						<div class="form-group">
							<select class="form-control input-lg">
								<option>All Categories</option>
								<option>Accounting, Banking &amp; Legal</option>
								<option>Building &amp; Trades</option>
								<option>Civil</option>
								<option>Cleaning &amp; Facility Management</option>
								<option>Engineering Consultants</option>
								<option>Electrical &amp; Automation</option>
								<option>General, Property, Auctions</option>
								<option>IT &amp; Telecoms</option>
								<option>Materials, Supply &amp; Services</option>
								<option>Mechanical, Plant &amp; Equipment</option>
								<option>Media &amp; Marketing</option>
								<option>Medical &amp; Healthcare</option>
								<option>Security, Access, Alarms, Fire</option>
							</select>
						</div>

						<div class="form-group">
							<select class="form-control input-lg">
								<option>All Locations</option>
								<option>Eastern Cape</option>
								<option>Free State</option>
								<option>Gauteng</option>
								<option>Kwazulu-Natal</option>
								<option>Limpopo</option>
								<option>     Mpumalanga</option>
								<option>Northen Cape</option>
								<option>North West</option>
								<option>Western Cape</option>
                       	    </select>
						</div>

						<div class="col-xs-6 col-xs-offset-3">
							<div class="form-group">
								<a href="cart.html" class="cart-btn input-lg">
									<i class="fa fa-search"></i>
									<span>Search</span></a>
							</div>
						</div>
					</form>
					<!-- form horizontal end -->

				</div>
			</div>
		</div>
	</section>
	<!-- search end-->
	
	<!-- popular categories start -->
	<section class="popular-categories text-center">
		<div class="container">
			<div class="row">
				<div class=" col-xs-12 pop">
					<h2>Popular Categories</h2>
				</div>
				
				
				<div class="col-xs-6 col-md-3 pop-section">
				<div class="pop-out">
					<p class="lead">Consulting in finance, insurance, IT etc.</p>
				</div>
					<img class="img-responsive center-block" src="images/consulting.png" alt="Consulting services">
					<h5>CONSULTANTS</h5>
				</div>
				
				
				<div class="col-xs-6 col-md-3 pop-section">
				<div class="pop-out">
					<p class="lead">Consulting in finance, insurance, IT etc.</p>
				</div>
				<div class="pop-out">
					<p class="lead">Consulting in finance, insurance, IT etc.</p>
				</div>
					<img class="img-responsive center-block" src="images/building.png" alt="BUILDING & TRADE services">
					<h5>BUILDING &amp; TRADE</h5>
				</div>
				
				
				<div class="col-xs-6 col-md-3 pop-section">
				<div class="pop-out">
					<p class="lead">Consulting in finance, insurance, IT etc.</p>
				</div>
					<img class="img-responsive center-block" src="images/civil.png" alt="civil services">
					<h5>CIVIL</h5>
				</div>
				
				
				<div class="col-xs-6 col-md-3 pop-section">
				<div class="pop-out">
					<p class="lead">Consulting in finance, insurance, IT etc.</p>
				</div>
					<img class="img-responsive center-block" src="images/IT.png" alt="IT & telecom services">
					<h5>IT &amp; TELECOM</h5>
				</div>
				
				
				<div class="col-xs-6 col-md-3 pop-section">
				<div class="pop-out">
					<p class="lead">Consulting in finance, insurance, IT etc.</p>
				</div>
					<img class="img-responsive center-block" src="images/hr.png" alt="HR & TRAINING services">
					<h5>HR &amp; TRAINING</h5>
				</div>
				
				
				<div class="col-xs-6 col-md-3 pop-section">
				<div class="pop-out">
					<p class="lead">Consulting in finance, insurance, IT etc.</p>
				</div>
					<img class="img-responsive center-block" src="images/security.png" alt="SECURITY services">
					<h5>SECURITY</h5>
				</div>
				
				
				<div class="col-xs-6 col-md-3 pop-section">
				<div class="pop-out">
					<p class="lead">Consulting in finance, insurance, IT etc.</p>
				</div>
					<img class="img-responsive center-block" src="images/electricity.png" alt="electrical & automation services">
					<h5>ELECTRICAL &amp; AUTOMATION</h5>
				</div>
				
				
				<div class="col-xs-6 col-md-3 pop-section">
				<div class="pop-out">
					<p class="lead">Consulting in finance, insurance, IT etc.</p>
				</div>
					<img class="img-responsive center-block" src="images/media-and-marketing.png" alt="media and marketing services">
					<h5>MEDIA &amp; MARKETING</h5>
				</div>
			</div>
		</div>
	</section>
	<!-- popular categories end -->

	<!--how it works Area Start-->
	<div class="gallery-images">
		<div class="servive-block">
			<div class="container">
				<h2 class="text-center">How It Works</h2>
			</div>
		</div>
		<div class="container">
			<div class="row flex-container-first">
				<div class="col-xs-12 col-sm-12 col-md-4 text-center sign-up-first">
					<h4>SIGN UP</h4>
					<img src="images/signup.png">
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 text-center start-project">
					<h4>START A PROJECT</h4>

					<img src="images/startproject.png">
				</div>
				<div class="clearfix hidden-md hidden-lg"></div>
				<div class="col-xs-12 col-sm-12 col-md-4 text-center get-bids">
					<h4>GET BIDS</h4>
					<img src="images/getbids.png">
				</div>

			</div>
			<div class="row flex-container">
				<div class="col-xs-12 col-sm-12 col-md-4 text-center">
					<img src="images/signup001.png">
					<h4>SIGN UP</h4>

				</div>
				<div class="col-xs-12 col-sm-12 col-md-4 text-center">
					<img src="images/findproject.png">
					<h4>FIND A PROJECT</h4>

				</div>
				<div class="clearfix hidden-md hidden-lg"></div>
				<div class="col-xs-12 col-sm-12 col-md-4 text-center">
					<img src="images/startbidding.png">
					<h4>START BIDDING</h4>

				</div>

			</div>

			<div class="col-md-12 text-center">
				<a href="#" class="explore-btn">
					<i class="fa fa-compass "></i>&nbsp;&nbsp;
					<span>LEARN MORE</span>
				</a>
			</div>
		</div>

	</div>
	<!--how it works End-->

	<!-- latest tenders start -->
	<section class="tenders">
		<div class="container">
			<h2 class="text-center">latest tenders</h2>
			<!-- 1st callout start -->
			<div class="callout">
				<div class="add-information">
					<a href="view.html"><h4>Additional Information</h4></a>
					<hr>
					<a href="view.html"><p class="lead"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio iure nostrum aliquam consequuntur, reprehenderit corporis id odio quas quaerat, dolore impedit itaque deleniti. Quod voluptas, ut molestias assumenda ullam. In.</p></a>
				</div>
				<div class="row">
					<div class=" col-xs-4 col-md-2">
						<img class="img-responsive img-circle center-block" src="images/tenders-img.jpg" alt="">
					</div>
					<div class="col-xs-8 col-md-4">
						<h3>Renewable energy project</h3>
						<p class="lead">Project Owner : Lascar</p>
					</div>
						<div class="col-xs-6 col-md-2 days">
						<span class="glyphicon glyphicon-time"></span>
						<span>&nbsp;&nbsp;13 days left</span>
					</div>
					<div class="col-xs-6 col-md-2 location">
					<span class="glyphicon glyphicon-map-marker"></span>
						<span>&nbsp;Western Cape </span>
					</div>
					<div class="col-xs-12 col-md-2 col-md-offset-0 bidding">
						<h5>bidding range</h5>
						<p class="bid-range">$15 000 - $20 000</p>
					</div>
				</div>
			</div>
			<!-- 1st callout end -->

			<!-- 2nd callout start -->
		<div class="callout">
				<div class="add-information">
					<a href="view.html"><h4>Additional Information</h4></a>
					<hr>
					<a href="view.html"><p class="lead"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio iure nostrum aliquam consequuntur, reprehenderit corporis id odio quas quaerat, dolore impedit itaque deleniti. Quod voluptas, ut molestias assumenda ullam. In.</p></a>
				</div>
				<div class="row">
					<div class=" col-xs-4 col-md-2">
						<img class="img-responsive img-circle center-block" src="images/tenders-img.jpg" alt="">
					</div>
					<div class="col-xs-8 col-md-4">
						<h3>Renewable energy project</h3>
						<p class="lead">Project Owner : Lascar</p>
					</div>
						<div class="col-xs-6 col-md-2 days">
						<span class="glyphicon glyphicon-time"></span>
						<span>&nbsp;&nbsp;13 days left</span>
					</div>
					<div class="col-xs-6 col-md-2 location">
					<span class="glyphicon glyphicon-map-marker"></span>
						<span>&nbsp;Western Cape </span>
					</div>
					<div class="col-xs-12 col-md-2 col-md-offset-0 bidding">
						<h5>bidding range</h5>
						<p class="bid-range">$15 000 - $20 000</p>
					</div>
				</div>
			</div>
			<!-- 2nd callout end -->

			<!-- 3rd callout start -->
		<div class="callout">
				<div class="add-information">
					<a href="view.html"><h4>Additional Information</h4></a>
					<hr>
					<a href="view.html"><p class="lead"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio iure nostrum aliquam consequuntur, reprehenderit corporis id odio quas quaerat, dolore impedit itaque deleniti. Quod voluptas, ut molestias assumenda ullam. In.</p></a>
				</div>
				<div class="row">
					<div class=" col-xs-4 col-md-2">
						<img class="img-responsive img-circle center-block" src="images/tenders-img.jpg" alt="">
					</div>
					<div class="col-xs-8 col-md-4">
						<h3>Renewable energy project</h3>
						<p class="lead">Project Owner : Lascar</p>
					</div>
						<div class="col-xs-6 col-md-2 days">
						<span class="glyphicon glyphicon-time"></span>
						<span>&nbsp;&nbsp;13 days left</span>
					</div>
					<div class="col-xs-6 col-md-2 location">
					<span class="glyphicon glyphicon-map-marker"></span>
						<span>&nbsp;Western Cape </span>
					</div>
					<div class="col-xs-12 col-md-2 col-md-offset-0 bidding">
						<h5>bidding range</h5>
						<p class="bid-range">$15 000 - $20 000</p>
					</div>
				</div>
			</div>
			<!-- 3rd callout end -->

			<!-- 4th callout start -->
			<div class="callout">
				<div class="add-information">
					<a href="view.html"><h4>Additional Information</h4></a>
					<hr>
					<a href="view.html"><p class="lead"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio iure nostrum aliquam consequuntur, reprehenderit corporis id odio quas quaerat, dolore impedit itaque deleniti. Quod voluptas, ut molestias assumenda ullam. In.</p></a>
				</div>
				<div class="row">
					<div class=" col-xs-4 col-md-2">
						<img class="img-responsive img-circle center-block" src="images/tenders-img.jpg" alt="">
					</div>
					<div class="col-xs-8 col-md-4">
						<h3>Renewable energy project</h3>
						<p class="lead">Project Owner : Lascar</p>
					</div>
						<div class="col-xs-6 col-md-2 days">
						<span class="glyphicon glyphicon-time"></span>
						<span>&nbsp;&nbsp;13 days left</span>
					</div>
					<div class="col-xs-6 col-md-2 location">
					<span class="glyphicon glyphicon-map-marker"></span>
						<span>&nbsp;Western Cape </span>
					</div>
					<div class="col-xs-12 col-md-2 col-md-offset-0 bidding">
						<h5>bidding range</h5>
						<p class="bid-range">$15 000 - $20 000</p>
					</div>
				</div>
			</div>
			<div class="col-md-12 text-center">
				<a href="#" class="explore-btn">
					<i class="fa fa-compass "></i>&nbsp;&nbsp;
					<span>EXPLORE ALL</span>
				</a>
			</div>
		</div>
	</section>
	<!-- latest tenders end -->

	<!--Testimonials  start-->
	<section class="testimonials text-center">
		<div class="container">
			<!-- start carousel -->
			<div id="carousel-testmonials" class="carousel slide" data-ride="carousel">


				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">

						<img class="img-responsive center-block img-circle" src="images/testimonials/1.jpg" alt="Customer Image">
						<!--star rating start -->
						<p class="lead">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
						</p>
						<!--star rating end -->
						<h4>great help</h4>
						<p class="lead">
							With Help of Bidtend service, most importantly we are working with our Ideal clients. Service is very easy to use and effective. We always gets prompt response for our query regarding Tenders &amp; Tender Documents. We really appreciate their efforts and service.
						</p>
					</div>
					<div class="item">
						<img class="img-responsive center-block img-circle" src="images/testimonials/2.jpg" alt="Customer Image">
						<!--star rating start -->
						<p class="lead">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
						</p>
						<!--star rating end -->
						<h4>great experience</h4>
						<p class="lead">
							The experience using Bidtend is extremely good. The portal gives us a threadbare information on the tenders and projects. To sum up, the experience is extremely satisfying.
						</p>
					</div>
					<!--star rating end -->
					<div class="item">
						<!--star rating start -->
						<img class="img-responsive center-block img-circle" src="images/testimonials/3.jpg" alt="Customer Image">
						<p class="lead">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
						</p>
						<h4>very impressive</h4>
						<p class="lead">
							We are impressed with the services of Bidtend. Timing and quality have always met our Expectations and everything is communicated in a professional and timely manner. They are always willing to help us, which has made us a Loyal Customer of theirs. Keep it up the Great work!
						</p>
					</div>
				</div>
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#carousel-testmonials" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-testmonials" data-slide-to="1"></li>
					<li data-target="#carousel-testmonials" data-slide-to="2"></li>
				</ol>
			</div>
			<!-- end carousel -->
		</div>
	</section>
	<!--Testimonials  End-->

	<!-- to top start -->
	<section class="to-top">
		<span><i class="fa fa-arrow-up" aria-hidden="true"></i></span>
	</section>
	<!-- to top end -->
	@endsection