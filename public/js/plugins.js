/*jslint browser: true*/
/*global $, jQuery, alert*/

$(document).ready(function () {
	"use strict";

	// scroll to top button
	$(window).on("scroll", function () {
		
		if ($(window).scrollTop() >= 400) {
			$(".to-top").fadeIn(500);
		} else {
			$(".to-top").fadeOut(500);
		}
		
	});
	
	$(".to-top span").on("click", function () {
		$("html, body").animate({ scrollTop : 0}, 700);
	});
	$(function () {
		$('a[href="#topmenusearch"]').on('click', function (event) {
			event.preventDefault();
			$('#topmenusearch').addClass('open');
			$('#topmenusearch > form > input[type="search"]').focus();
		});

		$('#topmenusearch, #topmenusearch button.close').on('click keyup', function (event) {
			if (event.target === this || event.target.className === 'close' || event.keyCode === 27) {
				$(this).removeClass('open');
			}
		});
	});


	$(".label-info").on("click",function(){
		$(this).remove();
	});


	$("#company-picture").on("change",function(event){
		var tmppath = URL.createObjectURL(event.target.files[0]);
		$("#open_img").attr("src",tmppath);
		$("#open_img").css("display","block");
		$("#open_label").css("display","none");
		//var file_obj = document.getElementById("company-picture");

		//console.log(file_obj.value);
		
	});
	$('#drag_file_open').on("change",function(event){
		$("#drag_file").html(event.target.files[0].name);
		//var file_obj = document.getElementById("company-picture");

		//console.log(file_obj.value);
		
	});


	// input tags

	// var tags = $('#in_skill').inputTags({
	//     autocomplete: {
	//       values: ['jQuery', 'PHP', 'CSS', 'Bootstrap']
	//     },
	//     max:4,
  	// });





    $('#img-zone').on(
	    'dragover',
	    function(e) {
	        e.preventDefault();
	        e.stopPropagation();
	        console.log('dragover');
	    }
	);
	$('#img-zone').on(
	    'dragenter',
	    function(e) {
	        e.preventDefault();
	        e.stopPropagation();
	    }
	);
	$('#img-zone').on(
	    'drop',
	    function(e){
	        if(e.originalEvent.dataTransfer){
	            console.log('yes');
	            if(e.originalEvent.dataTransfer.files.length) {
	                e.preventDefault();
	                e.stopPropagation();
	                /*UPLOAD FILES HERE*/
	                $("#drag_file").html(e.originalEvent.dataTransfer.files[0].name);
	                console.log(e.originalEvent.dataTransfer.files[0]);
	            }
	        }
	    }
	);





	$(".orange-circle-button").click(function(){
		$(this).css("border","2px solid rgb(50,50,220)");
		$(this).css("width","8.5em");
		$(this).css("height","8.5em");

		// new addition
        $(this).find('.categoty-checkbox').attr('checked', true);
        $('#category-submit').attr('disabled', false);
	});


	// plan select

	$(".plan").click(function(){
		// reset radio check first
        $('.plan').find('input').attr('checked', false);

		$(".plan").css("border","0px");
		$(this).css("border","2px solid #e14931");
		$(this).find('input').attr('checked', true);
	});



	$(".method").click(function(){
		$(".method").removeClass("blue-border");
		$(this).addClass("blue-border");
	});
});

function onPlusSkill()
{
	var tags = $("#in_skill").val();
	if(tags == "") return;
    var exploded = tags.split(',');

    $.each(exploded, function(index, element){
    	//console.log(index, element);
        var htm="<span class='tag label label-info' id='"+element+"' onclick='removetag("+element+");'>"+element+" <span data-role='remove'>X</span></span>";
        $("#skill_tag_list").append(htm);
	});

}

function removetag( tag ) {
    $(tag).remove();
}

function myMap() {
	var mapOptions = {
	    center: new google.maps.LatLng(51.5, -0.12),
	    zoom: 10,
	    mapTypeId: google.maps.MapTypeId.HYBRID
	}
	var map = new google.maps.Map(document.getElementById("div_map"), mapOptions);
}
function onReset()
{
	$(".orange-circle-button").css("border","0px");
	$(".orange-circle-button").css("width","8em");
	$(".orange-circle-button").css("height","8em");

	$('.categoty-checkbox').attr('checked', false);
	$('#category-submit').attr('disabled', 'true');
}



function activecheck( index ) {
	$('#hiremenus').find('li a').removeClass('active');
	//console.log(index);
    $('#hiremenus li').eq(index).find('a').attr('class', 'active');
}

$('.readmorebtn').click(function (e){
    e.preventDefault();
    $(this).parent().find('span').css('display', 'block');
    $(this).css('display', 'none');
    $(this).parent().find('.readlessbtn').removeClass('hidden');
});
$('.readlessbtn').click(function (e){
    e.preventDefault();
    $(this).parent().find('span').css('display', 'none');
    $(this).addClass('hidden');
    $(this).parent().find('.readmorebtn').css('display', 'block');;
});


$(document).ready(function(){

// social share kit
    SocialShareKit.init();
});

// input tags
$("#in_skill").tagsinput();



// $('.datepicker').datetimepicker({
//     format: 'mm-dd-yyyy'
// });

