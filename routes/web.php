<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.index');
});

Route::get('registration', function () {
    return view('users.registration');
});


Route::get('/registration-category', function () {
    return view('users.registration-category');
});

Route::get('registration-plans', function () {
    return view('users.registration-plans');
});

Route::get('registration-payments', function () {
    return view('users.registration-payments');
});

Route::get('registration-complete', function () {
    return view('users.registration-complete');
});

// Authentication Routes...
Route::get('/user-login', [
  'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('login', [
  'as' => '',
  'uses' => 'Auth\LoginController@login'
]);
Route::post('/logout', [
  'as' => 'logout',
  'uses' => 'Auth\LoginController@logout'
]);

// Registration Routes...
Route::get('/register', [
  'as' => 'register',
  'uses' => 'Auth\RegisterController@showRegistrationForm'
]);

Route::post('/register', [
  'as' => '',
  'uses' => 'Auth\RegisterController@create'
]);

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
